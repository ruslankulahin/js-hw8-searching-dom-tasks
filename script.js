// ## Теоретичні питання
/* 
1. Опишіть своїми словами що таке Document Object Model (DOM)

    DOM – це деревоподібне відтворення веб-сайту, у вигляді об'єктів, вкладених один в одного. 
    Таке дерево потрібне для правильного відображення сайту 
    та внесення змін на сторінках за допомогою JavaScript

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

    innerHtml повертає весь текст, включаючи HTML-теги, що містяться в елементі.
    innerText повертає весь текст, який міститься в елементі та у всіх його дочірніх елементах 
    к звичайний текст без будь-якого форматування.

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
    За допомогою пошукових методів:
    getElementById() - звернення до елементу за унікальнім id;
    getElementsByName() - пошук серед елементів з атрибутом name, повертає список всіх елементів, 
                        чий атрибут name задовольняє запиту;
    getElementsByClassName() - звернення до всіх елементів з однаковою назвою классу;
    getElementsByTagName() - звернення до всіх елементів з однаковою назвою тегу;
    querySelector() - шукає та повертає перший елемент, що задовольняє даному CSS-селектору;
    querySelectorAll() - повертає всі елементи, що задовольняють даномму CSS-селектору.

    Частіше використовуються методи querySelector() та querySelectorAll(). 
*/

// ## Завдання

// 1) Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const paragraphs = document.querySelectorAll('p');
paragraphs.forEach((el) => {
    el.style.backgroundColor = '#ff0000';
});
 
// 2) Знайти елемент із id='optionsList'. Вивести у консоль.
const startElement = document.getElementById('optionsList');
console.log(startElement);

//Знайти батьківський елемент та вивести в консоль.
const parent = startElement.parentNode;
console.log(parent);

//Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
if (startElement.hasChildNodes()) {
  let childrens = startElement.childNodes;
  for (let i = 0; i < childrens.length; ++i) {
    console.log(
      `Node ${i + 1} - name: ${startElement.childNodes[i].nodeName}, type: ${
        startElement.childNodes[i].nodeType
      }`
    );
  }
}

// 3) Встановіть в якості контента елемента з класом testParagraph наступний параграф - 
//    <p>This is a paragraph<p/>

 const element = document.getElementsByClassName('testParagraph');
 console.log(element);
// В консоль вивелось значення: HTMLCollection [] - пуста коллекція, 
// тобто елемент з классом testParagraph відсутній.
// Але є елемент з id testParagraph. Якщо це не заборонено завданням, то можна змінити так:
const newElement = document.getElementById('testParagraph');
newElement.innerText = 'This is a paragraph';

// 4) Отримати елементи <li>, вкладені в елемент із класом main-header і вивести їх у консоль.
//    Кожному з елементів присвоїти новий клас nav-item.

let mainHeaderItemsLi = document.querySelector('.main-header').querySelectorAll('li');
console.log(mainHeaderItemsLi);
for (let i = 0; i < mainHeaderItemsLi.length; i++) {
    mainHeaderItemsLi[i].className = 'nav-item'; 
}

// 5) Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const elemSectionTitle = document.querySelectorAll(".section-title");
for (let elem of elemSectionTitle) {
    elem.classList.remove("section-title");
}
     